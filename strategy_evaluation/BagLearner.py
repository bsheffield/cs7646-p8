"""  		   	  			    		  		  		    	 		 		   		 		  
A simple wrapper for linear regression.  (c) 2015 Tucker Balch  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---

Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

import numpy as np
from scipy import stats
import RTLearner as rtl


class BagLearner(object):

    def __init__(self, learner=rtl.RTLearner, leaf_size=1, kwargs={"leaf_size:1"}, bags=20, boost=False, verbose=False):
        """
        Bootstrap Aggregating Learner
        :param learner: Specify which learner to use:  LinRegLearner, DTLearner, RTLearner, or BagLearner
        :param leaf_size: maximum number of samples to be aggregated at a leaf.
        :param kwargs:
        :param bags:
        :param boost:
        :param verbose: print out information for debugging
        """

        self.verbose = verbose
        self.learners = list()
        for i in range(bags):
            self.learners.append(learner())
        self.bags = bags
        self.leaf_size = leaf_size
        self.boost = boost

    def author(self):
        """
        Returns Georgia Tech username.
        :return:
        """

        return 'bsheffield7'

    def addEvidence(self, dataX, dataY):
        """  		   	  			    		  		  		    	 		 		   		 		  
        @summary: Add training data to learner  		   	  			    		  		  		    	 		 		   		 		  
        @param dataX: X values of data to add  		   	  			    		  		  		    	 		 		   		 		  
        @param dataY: the Y training values  		   	  			    		  		  		    	 		 		   		 		  
        """

        samples_size = dataX.shape[0]
        for learner in self.learners:
            i = np.random.choice(samples_size, samples_size)
            learner.add_evidence(dataX[i], dataY[i])

    def add_evidence(self, dataX, dataY):
        self.addEvidence(dataX, dataY)

    def query(self, points):
        """  		   	  			    		  		  		    	 		 		   		 		  
        @summary: Estimate a set of test points given the model we built.  		   	  			    		  		  		    	 		 		   		 		  
        @param points: should be a numpy array with each row corresponding to a specific query.  		   	  			    		  		  		    	 		 		   		 		  
        @returns the estimated values according to the saved model.  		   	  			    		  		  		    	 		 		   		 		  
        """

        #Return the statistical mode instead of average
        #return np.mean(np.array([learner.query(points) for learner in self.learners]), axis=0)
        mode_result = stats.mode(np.array([learner.query(points) for learner in self.learners]), axis=0)
        return mode_result[0][0]
