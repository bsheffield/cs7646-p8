import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import indicators
from marketsimcode import compute_portfolio_stats, compute_portvals
from util import get_data

def testPolicy(symbol="AAPL", sd=dt.datetime(2010, 1, 1), ed=dt.datetime(2011, 12, 31), sv=100000):
    """
    Assume that you can see the future, but that you are constrained by the portfolio
    size and order limits as specified above. Create a set of trades that represents the
    best a strategy could possibly do during the in sample period. The reason we're
    having you do this is so that you will have an idea of an upper bound on
    performance.
    The intent is for you to use adjusted close prices with the market simulator that
    you wrote earlier in the course. For this activity, use $0.00, and 0.0 for
    commissions and impact respectively.
    Provide a chart that reports:
    Benchmark (see definition above) normalized to 1.0 at the start: Green line
    Value of the theoretically optimal portfolio (normalized to 1.0 at the start):
    Red line
    You should also report in text:
    Cumulative return of the benchmark and portfolio
    Stdev of daily returns of benchmark and portfolio
    Mean of daily returns of benchmark and portfolio

    Starting cash is $100,000.
    Allowable positions are: 1000 shares long, 1000 shares short, 0 shares.
    Benchmark: The performance of a portfolio starting with $100,000 cash,
    investing in 1000 shares of JPM and holding that position.
    There is no limit on leverage.
    Transaction costs for TheoreticallyOptimalStrategy: Commission: $0.00,
    Impact: 0.00.

    :param symbol: stock symbol to act on
    :param sd: a datetime object that represents the start date
    :param ed: a datetime object that represents the end date
    :param sv: start value of the portfolio
    :return: df_trades: A data frame whose values represent trades for each day. Legal
            values are +1000.0 indicating a BUY of 1000 shares, -1000.0 indicating a
            SELL of 1000 shares, and 0.0 indicating NOTHING. Values of +2000 and
            -2000 for trades are also legal so long as net holdings are constrained to
            -1000, 0, and 1000.
    """

    price_df = get_data([symbol], pd.date_range(sd, ed))
    stock_df = price_df[symbol]

    trade_decisions = list()

    sma_df = indicators.simple_moving_average(stock_df)

    #Use Bollinger Bands, Momentum, and MACD to produce trade signals
    bb_df = indicators.bollinger_bands(stock_df,sma_df)
    bbp_vals = bb_df["BBP"]
    bbp_vals = bbp_vals.fillna(0)

    momentum_df = indicators.momentum(stock_df)
    mm_vals = momentum_df["Momentum"]
    mm_vals = mm_vals.fillna(0)

    macd_df = indicators.macd(stock_df)
    macd_signals = macd_df["MACD_signal"]
    macd_signals = macd_signals.fillna(0)

    for i in range(len(bbp_vals.values)):
        signal = trade_signal(bbp_vals[i], mm_vals[i], macd_signals[i])
        trade_decisions.append(signal)

    trades = pd.DataFrame(0, index=stock_df.index, columns=["Shares"])
    #trades = list()
    num_shares = 0
    N = 5
    BUY=1
    SELL=-1

    for i in range(0, len(trade_decisions)):
        if trade_decisions[i] == BUY:
            if num_shares == 0:  # no position held previously
                num_shares = 1000
                #trades.append(1000)
                trades.values[i,:] = 1000
            elif num_shares == -1000:  # if we last had a short position
                num_shares = 1000
                #trades.append(2000)
                trades.values[i,:] = 2000
        if trade_decisions[i] == SELL:
            if num_shares == 0:  # no position held previously
                num_shares = -1000
                #trades.append(-1000)
                trades.values[i,:] = -1000
            elif num_shares == 1000:  # if we last had a long position
                num_shares = -1000
                #trades.append(-2000)
                trades.values[i,:] = -2000

    df_trades = pd.DataFrame(data=trade_decisions, index=price_df.index, columns=["Shares"])
    #print(df_trades)
    return trades

def trade_signal(bb, mm, macd):
    """
    Rule-based strategy to generate a trades dataframe over the in-sample period using 3 indicators. Dataframe will
    have the long, out, short positions with the respective values representing -1, 0, 1
    :return:
    """


    bb_buy_threshold = -1.3
    bb_sell_threshold = 1.0
    momentum_threshold = 0
    macd_threshold = 0
    BUY = 1
    SHORT = -1
    CASH = 0

    if bb < bb_buy_threshold and mm > momentum_threshold and macd > macd_threshold:
        return BUY
    elif bb > bb_sell_threshold and mm > momentum_threshold and macd > macd_threshold:
        return SHORT
    else:
        return CASH

def main(symbol="JPM",verbose=False):

    sd = dt.datetime(2008, 1, 1)
    ed = dt.datetime(2009, 12, 31)
    sv = 100000
    impact=0
    commission=0

    manual_strategy_orders = testPolicy(symbol=symbol, sd=sd, ed=ed, sv=sv)
    manual_strategy_df = compute_portvals(manual_strategy_orders,  sd=sd, ed=ed, symbol=symbol, impact=impact, commission=commission)
    manual_strategy_balance_df = manual_strategy_df['Balance']
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(manual_strategy_balance_df)
    manual_strategy_total_returns = manual_strategy_balance_df[-1]

    if verbose:
        print("Manual Strategy Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("Portfolio balance: " + str(round(manual_strategy_total_returns,4)))

    prices_df = get_data([symbol], pd.date_range(sd, ed))
    prices_df = prices_df[symbol]
    benchmark_df = np.zeros(len(prices_df.index))
    benchmark_df[0] = 1000 #hold 1000 shares of selected stock
    benchmark_df[-1] = -1000 #sell at end of period
    benchmark_df = pd.DataFrame(data=benchmark_df, index=prices_df.index, columns=['Shares'])
    benchmark_df = compute_portvals(benchmark_df, sd=sd, ed=ed, symbol=symbol, impact=impact, commission=commission )
    benchmark_balance_df = benchmark_df["Balance"]
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(benchmark_balance_df)
    benchmark_total_returns = benchmark_balance_df[-1]

    if verbose:
        print("BENCHMARK Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("Portfolio balance: " + str(round(benchmark_total_returns,4)))

    plt.title("Manual Strategy vs Benchmark")
    plt.xlabel("Dates")
    plt.ylabel("Portfolio Value")
    plt.plot(benchmark_balance_df, 'g', label="Benchmark")
    plt.plot(manual_strategy_balance_df, 'r', label="Manual Strategy")
    plt.legend()
    plt.savefig("Manual_Strategy_vs_Benchmark.png")


def author():
    return "bsheffield7"

if __name__ == "__main__":
    main(verbose=True)
