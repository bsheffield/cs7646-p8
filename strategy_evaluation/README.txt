# Project 8 - Strategy Evaluation

This README provides a brief synopsis of each file that was submitted for Project 8. It also contains the instructions
needed to reproduce results from report.pdf by informing the reader how to run the Python files.

## Files Submitted

- ```BagLearner.py``` - contains code for the classification Bag Learner class consisting on Random Trees.

- ```experiment1.py``` - Code conducting the experiment 1 outlined in Project 8 description

- ```experiment2.py``` - Code conducting the experiment 1 outlined in Project 8 description

- ```ManualStrategy.py``` -  Code implementing a ManualStrategy object. Implements testPolicy and calls marketsimcode.

- ```marketsimcode.py``` - Needed for ManualStrategy and StrategyLearner to simulate buy/sell trades on a
   particular stock symbol.

- ```RTLearner.py``` - Contains the code for the classifcation Random Tree class.

- ```StrategyLearner.py``` - Code implementing a StrategyLearner object

- ```testproject.py``` - code initializing/running all necessary files for the report.

## Instructions for running the Experiments

### Experiment 1

```experiment1.py``` can be ran directly with no arguments provided to the program. The symbol and the verbosity of
the program may be altered by either changing these variables directly. Another option of altering the symbol and
verbosity of the program is by supplying arguments to the program. The following is an example of how to change the
behavior of ```experiment1.py```:

Verbose = True

```bash
python experiment1.py -v
```

or

Verbose=True, impact = 0.05, and symbol = AAPL

```bash
python experiment1.py -v -s AAPL
```

### Experiment 2

```experiment2.py``` can be ran directly with no arguments provided to the program. The symbol, impact, and the verbosity of
the program may be altered by either changing these variables directly in the main function of the file. Another
option of altering the symbol and verbosity of the program is by supplying arguments to the program. The following
illustrates multiple examples of how to change the run-time behavior of ```experiment2.py```:

Verbose=False, impact = 0.5

```bash
python experiment2.py -v -i 0.5
```

or

Verbose=False, impact = 0.05, and symbol = UNH

```bash
python experiment2.py -i 0.05 -s UNH
```

### Test Project - Experiment 1 and 2

```testproject.py``` may be ran without any arguments directly for default behavior. There are two methods for altering
the run-time behavior of ```experiment1.py``` and ```experiment2.py``` for which it calls namely by changing the
hard-coded verbose, impact, and symbol variables or by supplying the program arguments. The following examples
demonstrate how one can run the file with supplied arguments:

Verbose = True, impact = 0.5

```bash
python testproject.py -v -i 0.5
```

or

Verbose = True, impact = 0.05, and symbol = ML4T-220

```bash
python testproject.py -v -i 0.05 -s ML4T-220
```

or

Verbose = False, impact = 0.05, symbol = SINE_FAST_NOISE

```bash
python testproject.py -i 0.05 -s SINE_FAST_NOISE
```
