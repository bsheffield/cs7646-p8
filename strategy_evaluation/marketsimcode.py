"""MC2-P1: Market simulator.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Template code for CS 4646/7646  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
We do grant permission to share solutions privately with non-students such  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
-----do not edit anything above this line---  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		     			  		 			     			  	  		 	  	 		 			  		  			
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

import datetime as dt
import pandas as pd
import numpy as np
from util import get_data

def compute_portvals(orders_file, sd=dt.datetime(2010, 1, 1), ed=dt.datetime(2011,12,31), symbol="JPM", start_val=100000, commission=0.0, impact=0.0):
    """
    # this is the function the autograder will call to test your code
    # NOTE: orders_file may be a string, or it may be a file object. Your
    # code should work correctly with either input
    :param orders_file: is the name of a file from which to read orders
    :param start_val: is the starting value of the portfolio (initial cash available)
    :param commission: is the fixed amount in dollars charged for each transaction (both entry and exit)
    :param impact: is the amount the price moves against the trader compared to the historical data at each transaction
    :return:
    """

    # read in adjusted closing prices for the equities
    #order_book_df = pd.read_csv(orders_file, index_col="Date", parse_dates=True, na_values=['nan'])

    #no longer reading an orders file but a csv file that has stock prices
    order_book_df = orders_file
    BUY = 1
    SELL = -1
    NA = 0

    start_date = sd
    end_date = ed
    dates = pd.date_range(start_date, end_date)

    symbols = [symbol]
    shares = 0

    stock_prices_df = get_data(symbols, dates, addSPY=False)
    COST = "Cost"

    cash_balance_df = order_book_df
    cash_balance_df[COST] = np.ones(cash_balance_df.shape[0])
    cash_balance_df = cash_balance_df * 0.0
    cash_balance_df[COST][0] = start_val

    stock_name = symbol
    for date_index, order in order_book_df.iterrows():
        order_price = stock_prices_df[stock_name].loc[date_index]
        order_units = abs(order[0])
        coefficient = NA
        if order[0] == 1000 or order[0] == 2000:
            coefficient = -1
            market_impact = order_units * order_price * impact
            cash_balance_df.loc[
                date_index, COST] += order_units * order_price * coefficient * -1 - commission - market_impact
            start_val = start_val - order_units * order_price * coefficient * -1 - commission - market_impact
            cash_balance_df.loc[date_index, "Balance"] = start_val
            shares = shares + order_units * coefficient * -1
            cash_balance_df.loc[date_index, "Total_Shares"] = shares
        elif order[0] == -1000 or order[0] == -2000:
            coefficient = 1
            market_impact = order_units * order_price * impact
            cash_balance_df.loc[
                date_index, COST] += order_units * order_price * coefficient * -1 - commission - market_impact
            start_val = start_val - order_units * order_price * coefficient * -1 - commission - market_impact
            cash_balance_df.loc[date_index, "Balance"] = start_val
            shares = shares + order_units * coefficient * -1
            cash_balance_df.loc[date_index, "Total_Shares"] = shares
        else:
            cash_balance_df.loc[date_index, COST] = 0.0
            cash_balance_df.loc[date_index, "Balance"] = start_val

        cash_balance_df.loc[date_index, "Shares"] += order[0]
        #shares = shares + order_units * coefficient * -1
        #cash_balance_df.loc[date_index, "Total_Shares"] = shares

    return cash_balance_df

def compute_portfolio_stats(portfolio_df, rfr=0.0, sf=252.0, sv=100000):
    """
    Returns common portfolio statistics based on current portfolio.
    :param portfolio_df: Dataframe
    :param rfr: Risk free return assumed to be 0
    :param sf: Number of days traded assumed to be 252 days out of the year.
    :return:

    """

    daily_rets = (portfolio_df / portfolio_df.shift(1)) - 1
    daily_rets = daily_rets[1:]
    cumulative_ret = (portfolio_df.iloc[-1] - sv) / sv
    avg_daily_ret = daily_rets.mean()
    std_daily_ret = daily_rets.std()
    sharpe_ratio = np.sqrt(sf) * daily_rets.mean() / std_daily_ret

    return cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio

def author():
    return 'bsheffield7'

