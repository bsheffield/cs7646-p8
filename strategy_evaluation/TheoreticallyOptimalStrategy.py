import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
from marketsimcode import compute_portfolio_stats, compute_portvals, total_daily_returns
from util import get_data


def testPolicy(symbol="AAPL", sd=dt.datetime(2010, 1, 1), ed=dt.datetime(2011, 12, 31), sv=100000):
    """
    Assume that you can see the future, but that you are constrained by the portfolio
    size and order limits as specified above. Create a set of trades that represents the
    best a strategy could possibly do during the in sample period. The reason we're
    having you do this is so that you will have an idea of an upper bound on
    performance.
    The intent is for you to use adjusted close prices with the market simulator that
    you wrote earlier in the course. For this activity, use $0.00, and 0.0 for
    commissions and impact respectively.
    Provide a chart that reports:
    Benchmark (see definition above) normalized to 1.0 at the start: Green line
    Value of the theoretically optimal portfolio (normalized to 1.0 at the start):
    Red line
    You should also report in text:
    Cumulative return of the benchmark and portfolio
    Stdev of daily returns of benchmark and portfolio
    Mean of daily returns of benchmark and portfolio

    Starting cash is $100,000.
    Allowable positions are: 1000 shares long, 1000 shares short, 0 shares.
    Benchmark: The performance of a portfolio starting with $100,000 cash,
    investing in 1000 shares of JPM and holding that position.
    There is no limit on leverage.
    Transaction costs for TheoreticallyOptimalStrategy: Commission: $0.00,
    Impact: 0.00.

    :param symbol: stock symbol to act on
    :param sd: a datetime object that represents the start date
    :param ed: a datetime object that represents the end date
    :param sv: start value of the portfolio
    :return: df_trades: A data frame whose values represent trades for each day. Legal
            values are +1000.0 indicating a BUY of 1000 shares, -1000.0 indicating a
            SELL of 1000 shares, and 0.0 indicating NOTHING. Values of +2000 and
            -2000 for trades are also legal so long as net holdings are constrained to
            -1000, 0, and 1000.
    """
    price_df = get_data([symbol], pd.date_range(sd, ed))
    stock_df = price_df[symbol]

    # by peaking into future, this list will represent the optimal
    # number of trade decisions to make
    trade_decisions = list()

    for i in range(len(price_df.index) - 1):
        future_stock_price = stock_df[i + 1]

        # peek into future to see if stock rises or falls to determine action
        if stock_df[i] < future_stock_price:
            # buy stock
            trade_decisions.append(1000)
        elif stock_df[i] > future_stock_price:
            # sell stock
            trade_decisions.append(-1000)
        else:
            trade_decisions.append(0)

    # represents last day where we can't see anymore into the future
    trade_decisions.append(0)
    df_trades = pd.DataFrame(data=trade_decisions, index=price_df.index, columns=["Shares"])
    return df_trades

def main(symbol="JPM",verbose=False):

    sd = dt.datetime(2008, 1, 1)
    ed = dt.datetime(2009, 12, 31)

    tos_orders = testPolicy(symbol=symbol, sd=sd, ed=ed, sv=100000)
    tos_df = compute_portvals(tos_orders,  sd=sd, ed=ed, symbol=symbol, impact=0, commission=0)
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(tos_df)
    tos_total_returns = total_daily_returns(tos_df)
    tos_total_returns_normalized = tos_total_returns/tos_total_returns.iloc[0]
    if verbose:
        print("TOS Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret[0],4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret[0],4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret[0],4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio[0],4)))

    #print(tos_df)

    prices_df = get_data([symbol], pd.date_range(sd, ed))
    prices_df = prices_df[symbol]
    benchmark_df = np.zeros(len(prices_df.index))
    benchmark_df[0] = 1000 #hold 1000 shares of selected stock e.g. JPM
    benchmark_df = pd.DataFrame(data=benchmark_df, index=prices_df.index, columns=['Benchmark'])
    benchmark_df = compute_portvals(benchmark_df, sd=sd, ed=ed, symbol=symbol, impact=0, commission=0 )
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(benchmark_df)
    benchmark_total_returns = total_daily_returns(benchmark_df)
    benchmark_total_returns_normalized = benchmark_total_returns/benchmark_total_returns.iloc[0]
    #print(benchmark_total_returns_normalized)

    if verbose:
        print("BENCHMARK Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret[0],4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret[0],4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret[0],4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio[0],4)))

    plt.title("TOS vs Benchmark")
    plt.xlabel("Dates")
    plt.ylabel("Normalized Portfolio Value")
    plt.plot(benchmark_total_returns_normalized, 'g', label="Benchmark")
    plt.plot(tos_total_returns_normalized, 'r', label="TOS")
    plt.legend()
    plt.savefig("TOS_vs_Benchmark.png")


def author():
    return "bsheffield7"

if __name__ == "__main__":
    main(verbose=False)
