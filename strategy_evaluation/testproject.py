

import getopt
import sys
import experiment1
import experiment2

if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "vsi:")

    verbose=False
    symbol="JPM"
    impact=0.005

    for opt, arg in opts:
        if opt == '-v':
            print("Verbose option provided")
            verbose=True
        if opt == 's':
            symbol = arg
        if opt == 'i':
            impact = arg

    if len(opts) < 1:
        experiment1.main(symbol=symbol, verbose=False)
        experiment2.main(symbol=symbol, impact=impact, verbose=verbose)
    else:
        experiment1.main(symbol=symbol, verbose=verbose)
        experiment2.main(symbol=symbol, impact=impact, verbose=verbose)

def author():
    return "bsheffield7"

