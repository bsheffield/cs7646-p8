""""""
"""  		  	   		   	 			  		 			 	 	 		 		 	
Template for implementing StrategyLearner  (c) 2016 Tucker Balch  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		   	 			  		 			 	 	 		 		 	
Atlanta, Georgia 30332  		  	   		   	 			  		 			 	 	 		 		 	
All Rights Reserved  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
Template code for CS 4646/7646  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		   	 			  		 			 	 	 		 		 	
works, including solutions to the projects assigned in this course. Students  		  	   		   	 			  		 			 	 	 		 		 	
and other users of this template code are advised not to share it with others  		  	   		   	 			  		 			 	 	 		 		 	
or to make it available on publicly viewable websites including repositories  		  	   		   	 			  		 			 	 	 		 		 	
such as github and gitlab.  This copyright statement should not be removed  		  	   		   	 			  		 			 	 	 		 		 	
or edited.  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
We do grant permission to share solutions privately with non-students such  		  	   		   	 			  		 			 	 	 		 		 	
as potential employers. However, sharing with other current or future  		  	   		   	 			  		 			 	 	 		 		 	
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		   	 			  		 			 	 	 		 		 	
GT honor code violation.  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
-----do not edit anything above this line---  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988		  	   		   	 			  		 			 	 	 		 		 	
"""

import datetime as dt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import util as ut
import indicators
import BagLearner
import RTLearner

from marketsimcode import compute_portfolio_stats, compute_portvals

class StrategyLearner(object):
    """  		  	   		   	 			  		 			 	 	 		 		 	
    A strategy learner that can learn a trading policy using the same indicators used in ManualStrategy.  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
    :param verbose: If “verbose” is True, your code can print out information for debugging.  		  	   		   	 			  		 			 	 	 		 		 	
        If verbose = False your code should not generate ANY output.  		  	   		   	 			  		 			 	 	 		 		 	
    :type verbose: bool  		  	   		   	 			  		 			 	 	 		 		 	
    :param impact: The market impact of each transaction, defaults to 0.0  		  	   		   	 			  		 			 	 	 		 		 	
    :type impact: float  		  	   		   	 			  		 			 	 	 		 		 	
    :param commission: The commission amount charged, defaults to 0.0  		  	   		   	 			  		 			 	 	 		 		 	
    :type commission: float  		  	   		   	 			  		 			 	 	 		 		 	
    """

    # constructor  		  	   		   	 			  		 			 	 	 		 		 	
    def __init__(self, verbose=False, impact=0.0, commission=0.0):
        """  		  	   		   	 			  		 			 	 	 		 		 	
        Constructor method  		  	   		   	 			  		 			 	 	 		 		 	
        """
        self.verbose = verbose
        self.impact = impact
        self.commission = commission
        self.learner = BagLearner.BagLearner(learner=RTLearner.RTLearner, kwargs={"leaf_size" : 10}, bags=20,
                                             boost=False, verbose=verbose)
        self.BUY = 1
        self.SELL = -1
        self.CASH = 0

    # this method should create a QLearner, and train it for trading  		  	   		   	 			  		 			 	 	 		 		 	
    def add_evidence(
            self,
            symbol="IBM",
            sd=dt.datetime(2008, 1, 1),
            ed=dt.datetime(2009, 1, 1),
            sv=100000,
    ):
        """  		  	   		   	 			  		 			 	 	 		 		 	
        Trains your strategy learner over a given time frame.  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
        :param symbol: The stock symbol to train on  		  	   		   	 			  		 			 	 	 		 		 	
        :type symbol: str  		  	   		   	 			  		 			 	 	 		 		 	
        :param sd: A datetime object that represents the start date, defaults to 1/1/2008  		  	   		   	 			  		 			 	 	 		 		 	
        :type sd: datetime  		  	   		   	 			  		 			 	 	 		 		 	
        :param ed: A datetime object that represents the end date, defaults to 1/1/2009  		  	   		   	 			  		 			 	 	 		 		 	
        :type ed: datetime  		  	   		   	 			  		 			 	 	 		 		 	
        :param sv: The starting value of the portfolio  		  	   		   	 			  		 			 	 	 		 		 	
        :type sv: int  		  	   		   	 			  		 			 	 	 		 		 	
        """

        # add your code to do learning here  		  	   		   	 			  		 			 	 	 		 		 	

        syms = [symbol]
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        prices = prices_all[syms]  # only portfolio symbols
        indicator_values = prices_all[syms]  # only portfolio symbols
        trainY = list()
        threshold = 0.02
        YBUY = threshold + self.impact # classify as LONG if it exceeds this value
        YSELL = -threshold - self.impact # classify as SHORT if it is below this value
        N = 5 # represents N day of return that Y classifications will be based on

        if self.verbose:
            print(indicator_values)

        indicator_values = indicators.combined_indicators(indicator_values)
        trainX = indicator_values[:-N]
        trainX = trainX.values

        # example use with new colname  		  	   		   	 			  		 			 	 	 		 		 	
        '''volume_all = ut.get_data(
            syms, dates, colname="Volume"
        )  # automatically adds SPY  		  	   		   	 			  		 			 	 	 		 		 	
        volume = volume_all[syms]  # only portfolio symbols  		  	   		   	 			  		 			 	 	 		 		 	
        volume_SPY = volume_all["SPY"]  # only SPY, for comparison later  		  	   		   	 			  		 			 	 	 		 		 	
        if self.verbose:
            print(volume)'''

        price = prices[symbol]
        # Use future N day for y training then classify as LONG,SHORT, or CASH. Predict a relative change
        # that we can use to invest with.
        for t in range(len(price.index) - N):
            #Pseudo code source: http://lucylabs.gatech.edu/ml4t/summer2020/classification-trader-hints/
            '''
            ret = (price[t + N] / price[t]) – 1.0
            if ret > YBUY:
                Y[t] = +1  # LONG
            else if ret < YSELL:
                Y[t] = -1  # SHORT
            else:
            Y[t] = 0  # CASH
            '''

            ret = (price[t + N] / price[t]) - 1.0
            if ret > YBUY:
                trainY.append(self.BUY) # LONG
            elif ret < YSELL:
                trainY.append(self.SELL) # SHORT
            else:
                trainY.append(self.CASH) # CASH

        trainY = np.array(trainY)

        self.learner.addEvidence(trainX, trainY)

    # this method should use the existing policy and test it against new data
    def testPolicy(
            self,
            symbol="IBM",
            sd=dt.datetime(2009, 1, 1),
            ed=dt.datetime(2010, 1, 1),
            sv=10000,
    ):
        """  		  	   		   	 			  		 			 	 	 		 		 	
        Tests your learner using data outside of the training data  		  	   		   	 			  		 			 	 	 		 		 	
  		  	   		   	 			  		 			 	 	 		 		 	
        :param symbol: The stock symbol that you trained on on  		  	   		   	 			  		 			 	 	 		 		 	
        :type symbol: str  		  	   		   	 			  		 			 	 	 		 		 	
        :param sd: A datetime object that represents the start date, defaults to 1/1/2008  		  	   		   	 			  		 			 	 	 		 		 	
        :type sd: datetime  		  	   		   	 			  		 			 	 	 		 		 	
        :param ed: A datetime object that represents the end date, defaults to 1/1/2009  		  	   		   	 			  		 			 	 	 		 		 	
        :type ed: datetime  		  	   		   	 			  		 			 	 	 		 		 	
        :param sv: The starting value of the portfolio  		  	   		   	 			  		 			 	 	 		 		 	
        :type sv: int  		  	   		   	 			  		 			 	 	 		 		 	
        :return: A DataFrame with values representing trades for each day. Legal values are +1000.0 indicating  		  	   		   	 			  		 			 	 	 		 		 	
            a BUY of 1000 shares, -1000.0 indicating a SELL of 1000 shares, and 0.0 indicating NOTHING.  		  	   		   	 			  		 			 	 	 		 		 	
            Values of +2000 and -2000 for trades are also legal when switching from long to short or short to  		  	   		   	 			  		 			 	 	 		 		 	
            long so long as net holdings are constrained to -1000, 0, and 1000.  		  	   		   	 			  		 			 	 	 		 		 	
        :rtype: pandas.DataFrame  		  	   		   	 			  		 			 	 	 		 		 	
        """

        # here we build a fake set of trades  		  	   		   	 			  		 			 	 	 		 		 	
        # your code should return the same sort of data
        '''dates = pd.date_range(sd, ed)
        prices_all = ut.get_data([symbol], dates)  # automatically adds SPY
        trades = prices_all[[symbol,]]  # only portfolio symbols
        trades_SPY = prices_all["SPY"]  # only SPY, for comparison later
        trades.values[:, :] = 0  # set them all to nothing
        trades.values[0, :] = 1000  # add a BUY at the start
        trades.values[40, :] = -1000  # add a SELL
        trades.values[41, :] = 1000  # add a BUY
        trades.values[60, :] = -2000  # go short from long
        trades.values[61, :] = 2000  # go long from short
        trades.values[-1, :] = -1000  # exit on the last day
        if self.verbose:
            print(type(trades))  # it better be a DataFrame!
        if self.verbose:
            print(trades)
        if self.verbose:
            print(prices_all)
        return trades'''

        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data([symbol], dates)  # automatically adds SPY
        trades = prices_all[symbol]  # only portfolio symbols
        #trades_SPY = prices_all["SPY"]  # only SPY, for comparison later

        testX = indicators.combined_indicators(trades)
        testX = testX.values
        testY = self.learner.query(testX)

        trades = pd.DataFrame(0, index=trades.index, columns=["Shares"])
        num_shares = 0
        N = 5

        for i in range(0, len(trades) - N):
            if testY[i] == self.BUY:
                if num_shares == 0: #no position held previously
                    num_shares = 1000
                    trades.iloc[i, 0] = 1000 #BUY of 1000 shares
                elif num_shares == -1000: #if we last had a short position
                    num_shares = 1000
                    trades.iloc[i, 0] = 2000 #short to long position
                #trades.iloc[i, 0] = 1000
            if testY[i] == self.SELL:
                if num_shares == 0: #no position held previously
                    num_shares = -1000
                    trades.iloc[i, 0] = -1000 # SELL OF 1000 shares
                elif num_shares == 1000: # if we last had a long position
                    num_shares = -1000
                    trades.iloc[i, 0] = -2000 #long to short position
                #trades.iloc[i, 0] = -1000

        return trades

def main(verbose=False):

    symbol = "JPM"
    st = StrategyLearner()
    sd = dt.datetime(2008, 1, 1)
    ed = dt.datetime(2009, 12, 31)
    impact = 0.0
    commission = 0.0

    st.add_evidence(symbol=symbol, sd=sd, ed=ed, sv=100000)
    test_trades = st.testPolicy(symbol=symbol, sd=sd, ed=ed, sv=100000)

    test_port_val = compute_portvals(test_trades, sd, ed, symbol=symbol, start_val=100000,
                                     commission=commission, impact=impact)
    test_port_val_balance = test_port_val['Balance']
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(test_port_val_balance)

    if verbose:
        print("Strategy Learner Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))

    prices_df = ut.get_data([symbol], pd.date_range(sd, ed))
    prices_df = prices_df[symbol]
    benchmark_df = np.zeros(len(prices_df.index))
    benchmark_df[0] = 1 #hold 1000 shares of selected stock
    benchmark_df[-1] = -1 #sell at end of period
    benchmark_df = pd.DataFrame(data=benchmark_df, index=prices_df.index, columns=['Shares'])
    benchmark_df = compute_portvals(benchmark_df, sd=sd, ed=ed, symbol=symbol, impact=impact, commission=commission )
    benchmark_balance_df = benchmark_df["Balance"]
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(benchmark_balance_df)
    benchmark_total_returns = benchmark_balance_df[-1]

    if verbose:
        print("BENCHMARK Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))

    plt.title("Strategy Learner vs Benchmark")
    plt.xlabel("Dates")
    plt.ylabel("Portfolio Value")
    plt.plot(benchmark_balance_df, 'g', label="Benchmark")
    plt.plot(test_port_val_balance, 'r', label="Strategy Learner")
    plt.legend()
    plt.savefig("Strategy_Learner_vs_Benchmark.png")

def author():
    return "bsheffield7"

if __name__ == "__main__":
    #print("One does not simply think up a strategy")
    main()
