
'''
Provide a hypothesis regarding how changing the value of impact should affect in-sample
trading behavior and results (provide at least two metrics, assessing a minimum of 3 different
measurements for each metric).

Your descriptions should be stated clearly enough that an informed reader could conduct
the experiment and reproduce the results without referencing your code.
'''


import datetime as dt
import getopt
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import util as ut
from StrategyLearner import StrategyLearner
from ManualStrategy import testPolicy
from marketsimcode import compute_portfolio_stats, compute_portvals

def main(symbol="JPM",verbose=False, impact=0.005, commission=0.0):

    st = StrategyLearner()
    sd = dt.datetime(2008, 1, 1)
    ed = dt.datetime(2009, 12, 31)
    sv = 100000

    #Manual Strategy
    manual_strategy_orders = testPolicy(symbol=symbol, sd=sd, ed=ed, sv=sv)
    manual_strategy_df = compute_portvals(manual_strategy_orders,  sd=sd, ed=ed, symbol=symbol,
                                          impact=impact, commission=commission)
    manual_strategy_balance_df = manual_strategy_df['Balance']
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(manual_strategy_balance_df)
    manual_strategy_total_returns = manual_strategy_balance_df[-1]
    manual_strategy_balance_normalized_df = manual_strategy_balance_df/manual_strategy_balance_df.iloc[0]

    if verbose:
        print("In-Sample Performance")
        print("---------------------")

    if verbose:
        print("Manual Strategy Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("\tPortfolio balance: " + str(round(manual_strategy_total_returns,4)))

    #Benchmark
    prices_df = ut.get_data([symbol], pd.date_range(sd, ed))
    prices_df = prices_df[symbol]
    benchmark_df = np.zeros(len(prices_df.index))
    benchmark_df[0] = -1000 #hold 1000 shares of selected stock
    benchmark_df[-1] = 1000 #sell at end of period
    benchmark_df = pd.DataFrame(data=benchmark_df, index=prices_df.index, columns=['Shares'])
    benchmark_df = compute_portvals(benchmark_df, sd=sd, ed=ed, symbol=symbol, impact=impact, commission=commission)
    benchmark_balance_df = benchmark_df["Balance"]
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(benchmark_balance_df)
    benchmark_total_returns = benchmark_balance_df[-1]
    benchmark_balance_normalized_df = benchmark_balance_df/benchmark_balance_df.iloc[0]

    if verbose:
        print("BENCHMARK Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("\tPortfolio balance: " + str(round(benchmark_total_returns,4)))

    plt.title("In-Sample: Benchmark vs Manual Strategy Impact=" + str(impact))
    plt.xlabel("Dates")
    plt.ylabel("Portfolio Value")
    plt.plot(benchmark_balance_normalized_df, 'g', label="Benchmark")
    plt.plot(manual_strategy_balance_normalized_df, 'r', label="Manual Strategy")
    plt.legend()
    plt.savefig("In-Sample_Benchmark_vs_ManualStrategy.png")

    #Strategy Learner
    st.add_evidence(symbol=symbol, sd=sd, ed=ed, sv=sv)
    test_trades = st.testPolicy(symbol=symbol, sd=sd, ed=ed, sv=sv)

    test_port_val = compute_portvals(test_trades, sd, ed, symbol=symbol, start_val=sv,
                                     commission=commission, impact=impact)
    test_port_val_balance = test_port_val['Balance']
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(test_port_val_balance)
    test_port_val_port_balance = test_port_val_balance[-1]
    test_port_val_balance_normalized = test_port_val_balance/test_port_val_balance.iloc[0]

    if verbose:
        print("Strategy Learner Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("\tPortfolio balance: " + str(round(test_port_val_port_balance,4)))

    plt.clf()
    plt.xlabel("Dates")
    plt.ylabel("Portfolio Value")
    plt.plot(manual_strategy_balance_normalized_df, 'r', label="Manual Strategy")
    plt.title("In-Sample:Manual Strategy vs Strategy Learner with Impact=" + str(impact))
    plt.plot(test_port_val_balance_normalized, 'b', label="Strategy Learner")
    plt.legend()
    plt.savefig("In-Sample_ManualStrategy_vs_StrategyLearner.png")

    if verbose:
        print("\nOut-of-Sample Performance")
        print("---------------------")
    #Testing / out-of-sample: January 1, 2010 to December 31 2011.
    sd = dt.datetime(2010, 1, 1)
    ed = dt.datetime(2011, 12, 31)

    manual_strategy_orders = testPolicy(symbol=symbol, sd=sd, ed=ed, sv=sv)
    manual_strategy_df = compute_portvals(manual_strategy_orders,  sd=sd, ed=ed, symbol=symbol, impact=impact, commission=commission)
    manual_strategy_balance_df = manual_strategy_df['Balance']
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(manual_strategy_balance_df)
    manual_strategy_total_returns = manual_strategy_balance_df[-1]
    manual_strategy_balance_normalized_df = manual_strategy_balance_df/manual_strategy_balance_df.iloc[0]

    if verbose:
        print("Manual Strategy Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("\tPortfolio balance: " + str(round(manual_strategy_total_returns,4)))

    #Benchmark
    prices_df = ut.get_data([symbol], pd.date_range(sd, ed))
    prices_df = prices_df[symbol]
    benchmark_df = np.zeros(len(prices_df.index))
    benchmark_df[0] = -1000 #hold 1000 shares of selected stock
    benchmark_df[-1] = 1000 #sell at end of period
    benchmark_df = pd.DataFrame(data=benchmark_df, index=prices_df.index, columns=['Shares'])
    benchmark_df = compute_portvals(benchmark_df, sd=sd, ed=ed, symbol=symbol, start_val=sv,
                                    impact=impact, commission=commission)
    benchmark_balance_df = benchmark_df["Balance"]
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(benchmark_balance_df)
    benchmark_total_returns = benchmark_balance_df[-1]
    benchmark_balance_normalized_df = benchmark_balance_df/benchmark_balance_df.iloc[0]

    if verbose:
        print("BENCHMARK Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("\tPortfolio balance: " + str(round(benchmark_total_returns,4)))

    plt.clf()
    plt.title("Out-of-Sample: Benchmark vs Manual Strategy")
    plt.xlabel("Dates")
    plt.ylabel("Portfolio Value")
    plt.plot(benchmark_balance_normalized_df, 'g', label="Benchmark")
    plt.plot(manual_strategy_balance_normalized_df, 'r', label="Manual Strategy")
    plt.legend()
    plt.savefig("Out-Of-Sample_Benchmark_vs_ManualStrategy.png")

    #Strategy Learner Out-of-sample
    test_trades = st.testPolicy(symbol=symbol, sd=sd, ed=ed, sv=sv)
    test_port_val = compute_portvals(test_trades, sd=sd, ed=ed, symbol=symbol, start_val=sv,
                                     commission=commission, impact=impact)
    test_port_val_balance = test_port_val['Balance']
    cumulative_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = compute_portfolio_stats(test_port_val_balance)
    test_port_val_port_balance = test_port_val_balance[-1]
    test_port_val_balance_normalized = test_port_val_balance/test_port_val_balance.iloc[0]

    if verbose:
        print("Strategy Learner Portfolio Stats")
        print("\tCumulative Return: " + str(round(cumulative_ret,4)))
        print("\tAverage Daily Return: " + str(round(avg_daily_ret,4)))
        print("\tSTD Daily Return: " + str(round(std_daily_ret,4)))
        print("\tSharpe Ratio: " + str(round(sharpe_ratio,4)))
        print("\tPortfolio balance: " + str(round(test_port_val_port_balance,4)))

    plt.clf()
    plt.xlabel("Dates")
    plt.ylabel("Portfolio Value")
    plt.plot(benchmark_balance_normalized_df, 'g', label="Benchmark")
    plt.plot(manual_strategy_balance_normalized_df, 'r', label="Manual Strategy")
    plt.plot(test_port_val_balance_normalized, 'b', label="Strategy Learner")
    plt.legend()
    plt.title("Out-of-Sample: Benchmark vs Manual Strategy vs Strategy Learner")
    plt.savefig("Out-of-Sample_Benchmark_vs_ManualStrategy_vs_StrategyLearner.png")

def author():
    return "bsheffield7"

if __name__ == "__main__":
    opts, args = getopt.getopt(sys.argv[1:], "vsi:")

    verbose=True
    symbol="JPM"
    impact=0.005

    for opt, arg in opts:
        if opt == 'v':
            print("Verbose option provided")
            verbose=True
        if opt == 's':
            symbol = arg
        if opt == 'i':
            impact = arg

    main(symbol=symbol, impact=impact, verbose=verbose)
    main(symbol=symbol, impact=0.05, verbose=verbose)
    main(symbol=symbol, impact=0.5, verbose=verbose)