import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
from util import get_data
import sys


# Indicators chosen for Project 6:
# 1) Simple Moving Average (SMA),
# 2) Bollinger Bands,
# 3) Momentum,
# 4) Exponential Moving Average(EMA),
# 5) Moving Average Convergence Divergence(MACD)

def main(symbol="JPM"):
    """
    :return:
    """

    start_date = dt.datetime(2008, 1, 1)
    end_date = dt.datetime(2009, 12, 31)
    dates = pd.date_range(start_date, end_date)

    prices_df = get_data([symbol], dates)[symbol]
    prices_df = fill_values(prices_df)
    prices_norm_df = normalize(prices_df)

    sma_df = simple_moving_average(prices_norm_df)

    bb_df = bollinger_bands(prices_norm_df, sma_df)
    #print(bb_df)
    mm_df = momentum(prices_norm_df)
    ema_df = ema(prices_norm_df)
    macd_df = macd(prices_norm_df)

    # Indicator 1
    sma_plot = pd.concat([prices_norm_df, sma_df], axis=1)
    sma_plot.plot(grid=True, title='Simple Moving Average', use_index=True)
    plt.savefig('SMA.png')

    # Indicator 2
    bb_plot = pd.concat([prices_norm_df, bb_df['Bottom'], bb_df['Top'], bb_df['BBP']], axis=1)
    bb_plot.plot(grid=True, title='Bollinger Bands', use_index=True)
    plt.savefig('BB.png')

    # Indicator 3
    mm_plot = pd.concat([prices_norm_df, mm_df['Momentum']], axis=1)
    mm_plot.plot(grid=True, title='Momentum', use_index=True)
    plt.savefig('Momentum.png')

    # Indicator 4: Exponential Moving Average (EMA)
    ema_plot = pd.concat([prices_norm_df, ema_df['EMA']], axis=1)
    ema_plot.plot(grid=True, title='Exponential Moving Average', use_index=True)
    plt.savefig('EMA.png')

    # Indicator 5: Moving Average Convergence Divergence (MACD)
    macd_plot = pd.concat([prices_norm_df, macd_df['MACD'], macd_df['MACD_signal']], axis=1)
    macd_plot.plot(grid=True, title='Moving Average Convergence Divergence', use_index=True)
    plt.savefig('MACD.png')


def fill_values(prices):
    """

    :param prices:
    :return:
    """

    prices.fillna(method='ffill')
    prices.fillna(method='bfill')

    return prices


def normalize(prices):
    """

    :param prices:
    :return:
    """

    return prices / prices.iloc[0,]


def ema(prices, lookback=12):
    # source https://www.investopedia.com/terms/e/ema.asp
    # using the 12-day exponential moving average as it is popularly chosen

    ema_df = pd.DataFrame(0, index=prices.index, columns=['EMA'])
    ema_df['EMA'] = prices.ewm(com=(lookback - 1) / 2).mean()
    return ema_df


def macd(prices, period_short=12, period_long=26, period_signal=9):
    # source: https://www.investopedia.com/terms/m/macd.asp

    macd_df = pd.DataFrame(0, index=prices.index, columns=['MACD', 'MACD_signal'])
    macd_df['MACD'] = ema(prices, lookback=period_short) - ema(prices, lookback=period_long)
    macd_df['MACD_signal'] = macd_df['MACD'].ewm(ignore_na=False, min_periods=0, com=period_signal, adjust=True).mean()

    return macd_df

def simple_moving_average(prices, lookback=20):
    """
    Vectorized SMA.  Indicator #1
    source:  Decision Tree-based Trading 2 - https://www.youtube.com/watch?v=2e2Yr-Bpo-w

    :param prices:
    :param window:
    :return:
    """

    sma = pd.DataFrame(0, index=prices.index, columns=['SMA'])
    sma['SMA'] = prices.rolling(window=lookback, min_periods=lookback).mean()
    return sma


def bollinger_bands(prices, sma, lookback=20):
    """
    Vectorized BB.  Indicator #2
    source:  Decision Tree-based Trading 2 - https://www.youtube.com/watch?v=2e2Yr-Bpo-w

    :param prices:
    :param sma:
    :param window:
    :param sd:
    :return:
    """

    rolling_mean = prices.rolling(window=lookback).mean()
    rolling_std = prices.rolling(window=lookback).std()
    #top_band = sma['SMA'] + (2 * rolling_std)
    #bottom_band = sma['SMA'] - (2 * rolling_std)

    #bb_df = pd.DataFrame(0, index=prices.index, columns=['Top', 'Bottom', 'BBP'])
    bb_df = pd.DataFrame(0, index=prices.index, columns=['BBP'])
    #bb_df['Top'] = top_band
    #bb_df['Bottom'] = bottom_band
    bb_df['BBP'] = (prices - rolling_mean) / (2 * rolling_std)  # actionable signal to buy/sell for learner
    return bb_df


def momentum(prices, lookback=20):
    """
    Indicator #3
    momentum[t] = prices[t]/prices[t-lookback] - 1
    :param prices:
    :param lookback:
    :return:
    """

    momentum_df = pd.DataFrame(0, index=prices.index, columns=['Momentum'])
    momentum_df['Momentum'] = (prices / prices.shift(lookback)) - 1
    return momentum_df


def combined_indicators(prices):
    """
    Return a dataframe that contains all five indicators
    :param prices:
    :return:
    """

    sma_df = simple_moving_average(prices)
    bb_df = bollinger_bands(prices, sma_df)
    mm_df = momentum(prices)
    ema_df = ema(prices)
    macd_df = macd(prices)

    #df = sma_df.copy()
    #df = df.join(bb_df)
    #df = df.join(mm_df)
    #df = df.join(ema_df)
    #df = df.join(macd_df)
    #bb_df.drop(["BBP"], axis=1, inplace=True)
    df = pd.concat((bb_df, mm_df, macd_df), axis=1)

    #cleanup data before returning result
    df = fill_values(df)
    df.fillna(0, inplace=True)

    return df


def author():
    """
    :return:
    """

    return "bsheffield7"


if __name__ == "__main__":
    main(symbol="JPM")
